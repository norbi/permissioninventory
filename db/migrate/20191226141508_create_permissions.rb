class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.string :name
      t.references :resource, null: false, foreign_key: true

      t.timestamps
    end
  end
end
