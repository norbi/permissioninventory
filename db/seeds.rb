# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

puts 'Creating resources...'
resource_1 = Resource.create!(name: 'App01', location: '127.0.0.1')
resource_2 = Resource.create!(name: 'Dev01', location: '127.0.0.2')

puts 'Creating permissions...'
perm_1 = Permission.create!(name: 'admin', resource: resource_1)
perm_2 = Permission.create!(name: 'manager', resource: resource_1)
perm_3 = Permission.create!(name: 'guest', resource: resource_1)
perm_4 = Permission.create!(name: 'translator', resource: resource_2)
perm_5 = Permission.create!(name: 'root', resource: resource_2)

puts 'Creating roles...'
admin_role = Role.create!(name: 'admin')
guest_role = Role.create!(name: 'guest')
manager_role = Role.create!(name: 'manager')

puts 'Creating users...'
user_1 = User.create(first_name: 'Test', last_name: 'Elek', email: 'test.elek@pm.hu')
user_1.roles << admin_role
user_1.roles << manager_role

user_1.permissions << perm_5
user_1.permissions << perm_2

puts 'Creating 10_000 records...'
(1..10_000).each do |idx|
  resource_x = Resource.create!(name: Faker::Internet.domain_name, location: Faker::Internet.ip_v4_address)
  puts 'Creating permissions...'
  perm_x1 = Permission.create!(name: 'admin', resource: resource_x)
  perm_x2 = Permission.create!(name: 'manager', resource: resource_x)
  perm_x3 = Permission.create!(name: 'guest', resource: resource_x)

  user_x = User.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email)
  user_x.roles << admin_role
  user_x.roles << manager_role

  user_x.permissions << perm_x1
  user_x.permissions << perm_x2

  puts idx
end

puts 'Done.'
