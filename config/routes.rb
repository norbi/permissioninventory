Rails.application.routes.draw do
  get 'permission/index'
  resources :users
  resources :resources
  root 'resources#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
