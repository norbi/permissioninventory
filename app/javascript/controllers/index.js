// Load all the controllers within this directory and all subdirectories. 
// Controller files must be named *_controller.js.

import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"

const application = Application.start()
const context = require.context("controllers", true, /_controller\.js$/)
application.load(definitionsFromContext(context))



// If jQuery is not already registered in window.jQuery do it here.
window.jQuery = window.$ = require('jquery')
 
// The controller will call: window.jQuery(table).DataTable(config)
require('datatables.net')
 
// These examples use bootstrap4 and the scroller plugin.
// See https://datatables.net/download for more options.
require('datatables.net-bs')
require('datatables.net-scroller-bs')
 
 
// Register the rails-datatables controller.
import Datatable from 'rails-datatables'
application.register('datatable', Datatable)


import Select2Controller from 'stimulus-select2'
application.register("select2", Select2Controller)