class ResourceDatatable < AjaxDatatablesRails::ActiveRecord
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "Resource.id", cond: :eq },
      name: { source: "Resource.name", cond: :like },
      location: { source: "Resource.location", cond: :like },
      dt_actions: { source: 'dt_actions', searchable: false, orderable: false }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        id: record.id,
        name: record.name,
        location: record.location,
        DT_RowId:   record.id,
        dt_actions: record.decorate.dt_actions
      }
    end
  end

  def get_raw_records
    # insert query here
    # User.all
    Resource.all
  end

end

