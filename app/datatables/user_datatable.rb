class UserDatatable < AjaxDatatablesRails::ActiveRecord

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id: { source: "User.id", cond: :eq },
      first_name: { source: "User.first_name", cond: :like },
      last_name: { source: "User.last_name", cond: :like },
      email: { source: "User.email", cond: :like },
      dt_actions: { source: 'dt_actions', searchable: false, orderable: false }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        id: record.id,
        first_name: record.first_name,
        last_name: record.last_name,
        email: record.email,
        DT_RowId:   record.id,
        dt_actions: record.decorate.dt_actions
      }
    end
  end

  def get_raw_records
    # insert query here
    User.all
  end

end
