class ResourceDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

#end


#class ResourceDecorator < ApplicationDecorator
#  delegate :last_name, :bio

  def check_box
    h.check_box_tag 'resources[]', object.id
  end

  def link_to
    h.link_to object.name, h.edit_resource_path(object)
  end


  # Just an example of a complex method you can add to you decorator
  # To render it in a datatable just add a column 'dt_actions' in
  # 'view_columns' and 'data' methods and call record.decorate.dt_actions
  def dt_actions
    links = []
    links << h.link_to('Edit', h.edit_resource_path(object))# if h.policy(object).update?
    links << h.link_to('Delete', h.resource_path(object), method: :delete, remote: true) #if h.policy(object).destroy?
    h.safe_join(links, ' | ')
  end
end