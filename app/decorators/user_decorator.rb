class UserDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def dt_actions
    links = []
    links << h.link_to('Edit', h.edit_user_path(object))# if h.policy(object).update?
    links << h.link_to('Delete', h.user_path(object), method: :delete, remote: true) #if h.policy(object).destroy?
    h.safe_join(links, ' | ')
  end
end
