class User < ApplicationRecord
  has_and_belongs_to_many :roles
  # has_and_belongs_to_many :permissions
  has_many :permission_users
  has_many :permissions, through: :permission_users
  has_many :resources, through: :permissions
  accepts_nested_attributes_for :permission_users, reject_if: :all_blank, allow_destroy: true

  validates :first_name, :last_name, :email, presence: true
end
