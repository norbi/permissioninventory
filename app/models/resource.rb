class Resource < ApplicationRecord
  has_many :permissions
  accepts_nested_attributes_for :permissions, reject_if: :all_blank, allow_destroy: true
end
