class Permission < ApplicationRecord
  belongs_to :resource
  # has_and_belongs_to_many :users
  has_many :permissions_users
  has_many :users, through: :permissions_users

  def name_with_resource
    "#{resource.name} - #{name}"
  end
end
