json.extract! resource, :id, :name, :location, :description, :created_at, :updated_at
json.url resource_url(resource, format: :json)
