class PermissionController < ApplicationController
  def index
    @permissions = Permission
                       .joins(:resource)
                       .where('permissions.name ilike :term OR resources.name ilike :term', term: "%#{params[:q]}%")
                       .limit(Fluenta::CommonUtilities::Controllers::Select2::AUTOCOMPLETE_LIMIT)
                       .offset(offset_from_params)

    render json: @permissions,
           each_serializer: Select2::CommonSerializer,
           adapter: :json,
           text_attribute_name: 'name_with_resource',
           meta_key: 'pagination',
           meta: meta_attributes(@permissions)
  end
end
